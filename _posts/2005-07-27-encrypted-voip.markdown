---
layout: post
status: publish
published: true
title: Encrypted Voip
author:
  display_name: Jay
  login: jk3us
  email: jay@jayknight.com
  url: ''
author_login: jk3us
author_email: jay@jayknight.com
wordpress_id: 16
wordpress_url: http://jk3.us/?p=16
date: !binary |-
  MjAwNS0wNy0yNyAxNDo0NDozMiArMDAwMA==
date_gmt: !binary |-
  MjAwNS0wNy0yNyAxOTo0NDozMiArMDAwMA==
categories:
- Uncategorized
tags:
- voip
comments: []
---
<p>Wired news has a <a href="http://www.wired.com/news/technology/0,1282,68306,00.html">story</a> about Phil Zimmermann's plans to develop a program for encrypting Voip calls.  I definitely agree that there is a need for encrypted voip, but this is not so revolutionary.  All of Skype's calls are already encrypted and <a href="http://forum.gizmoproject.com/viewtopic.php?t=152">apparently</a> Gizmo now has the capability built into the software.  I am not sure whether there is much consumer voip hardware that supports encrypted SIP, but it can't be far off.  Is there still a need for Zimmerman to write (and sell) such a technology?</p>
