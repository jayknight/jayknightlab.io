---
layout: post
status: publish
published: true
title: Read-only select elements in HTML
author:
  display_name: Jay
  login: jk3us
  email: jay@jayknight.com
  url: ''
author_login: jk3us
author_email: jay@jayknight.com
wordpress_id: 41
wordpress_url: http://jk3.us/2006/07/06/read-only-select-elements-in-html/
date: !binary |-
  MjAwNi0wNy0wNiAxNjoxMTo1NSArMDAwMA==
date_gmt: !binary |-
  MjAwNi0wNy0wNiAyMjoxMTo1NSArMDAwMA==
categories:
- General
tags:
- html
- programming
- web
comments:
- id: 83
  author: Anon
  author_email: nope@example.com
  author_url: ''
  date: !binary |-
    MjAwOC0xMS0yNCAxNzozNjowNiArMDAwMA==
  date_gmt: !binary |-
    MjAwOC0xMS0yNCAyMjozNjowNiArMDAwMA==
  content: ! 'You can first disable the select in the HTML code, then enable them
    with javascript.


    Then you add a handler to the onChange-event or something. This handler will set
    the selected index back to its original value (and also output an error message
    saying that you can''t change the values)


    You could also make it more obvious that it''s read only by using CSS and give
    the elements a more grey look'
- id: 84
  author: josh
  author_email: a@b.cde
  author_url: ''
  date: !binary |-
    MjAwOC0xMS0yNCAxODo1NDo0NyArMDAwMA==
  date_gmt: !binary |-
    MjAwOC0xMS0yNCAyMzo1NDo0NyArMDAwMA==
  content: You could leave it disabled and duplicate it with a hidden field.
- id: 85
  author: igo
  author_email: shb@inbox.lv
  author_url: ''
  date: !binary |-
    MjAwOC0xMi0yOSAwODowMTo1MiArMDAwMA==
  date_gmt: !binary |-
    MjAwOC0xMi0yOSAxMzowMTo1MiArMDAwMA==
  content: ! 'josh++


    this approch is very useful in lots of situations. for example - read only checkboxes
    (if checkbox is marked readonly you can still check/uncheck it - value of control
    IS read only, but UI state not :D ).'
- id: 86
  author: Code Pug
  author_email: nickcecilyandeli@yahoo.com
  author_url: http://www.codepug.com
  date: !binary |-
    MjAwOS0wOS0zMCAwMDowNjoxMSArMDAwMA==
  date_gmt: !binary |-
    MjAwOS0wOS0zMCAwNTowNjoxMSArMDAwMA==
  content: ! 'I have created a JavaScript snippet that will allow you to make your
    HTML select controls read-only simply by giving them an ID attribute and calling
    the script available on my site at http://www.codepug.com/readonlySelect.html


    Happy coding!

    --X

    .'
- id: 243
  author: MightyPork
  author_email: ondra@ondrovo.com
  author_url: http://www.ondrovo.com
  date: !binary |-
    MjAxMi0wMy0yOSAxMjoxODo1NyArMDAwMA==
  date_gmt: !binary |-
    MjAxMi0wMy0yOSAxNzoxODo1NyArMDAwMA==
  content: What about hiding the Select behind a transparent div?
- id: 244
  author: MightyPork
  author_email: ondra@ondrovo.com
  author_url: http://www.ondrovo.com
  date: !binary |-
    MjAxMi0wMy0yOSAxMjoyMDoyMCArMDAwMA==
  date_gmt: !binary |-
    MjAxMi0wMy0yOSAxNzoyMDoyMCArMDAwMA==
  content: ! "First post didn't show the code successful, I hope this will work better:\n&lt;div
    style='display:inline-block; position:relative;'&gt;\n    &lt;div style='position:absolute;width:100%;height:100%;'&gt;&lt;/div&gt;\n
    \   &lt;select.../&gt;\n&lt;/div&gt;"
---
<p>In HTML 4, form elements can be defined as "<a title="Read-only Controls" href="http://www.w3.org/TR/1998/REC-html40-19980424/interact/forms.html#h-17.12.2">readonly</a>" so that the user can't modify the value. However, you'll notice that select controls (the drop-down lists) do not support the readonly attribute, just input and textarea controls. Can anyone out there please explain the rational behind this? It seems perfectly reasonable to have a readonly select box, especially if it is put into that state by a script.</p>
<p>I think my workaround will consist of making them disabled so they cannot be editted, and enabling them via a script right before the form submits so that the values for those controls are submitted (disabled controls are not <a title="Successful controls" href="http://www.w3.org/TR/1998/REC-html40-19980424/interact/forms.html#h-17.12.2">successful</a> and are not "valid for submission").</p>
<p>If anyone can explain why selects can't be made readonly, or give me a better solution for simulating readonly-ness, please share.</p>
