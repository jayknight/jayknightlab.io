---
layout: post
status: publish
published: true
title: Pay for next year&#039;s gas... now!
author:
  display_name: Jay
  login: jk3us
  email: jay@jayknight.com
  url: ''
author_login: jk3us
author_email: jay@jayknight.com
wordpress_id: 40
wordpress_url: http://jk3.us/2006/05/08/pay-for-next-years-gas-now/
date: !binary |-
  MjAwNi0wNS0wOCAxMzowMjozOSArMDAwMA==
date_gmt: !binary |-
  MjAwNi0wNS0wOCAxOTowMjozOSArMDAwMA==
categories:
- Uncategorized
tags:
- life
comments: []
---
<p>Ok, <a href="http://www2.firstfuelbank.com/">this is just cool</a>.  The idea is simple.  Pay for however much gas you want right now at current prices and go get that gas as you need it, without worrying about rising gas prices.  Apparently, this has been going on since 1982, but only in Minnesota.  Imagine next year when gas costs $4 per gallon, and you can go fill up at $2.69... sounds nice huh?  Of course, it may have gone up so much because people like you have been hoarding it all for themselves.  Regardless, this is neat, and I want them to expand to Mississippi.  (via <a href="http://www.cnn.com/2006/US/05/08/gas.bank.ap/index.html?section=cnn_topstories">CNN</a>)</p>
