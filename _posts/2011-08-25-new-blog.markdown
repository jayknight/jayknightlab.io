---
layout: post
status: publish
published: true
title: New Blog
author:
  display_name: Jay
  login: jk3us
  email: jay@jayknight.com
  url: ''
author_login: jk3us
author_email: jay@jayknight.com
wordpress_id: 186
wordpress_url: http://jk3.us/?p=186
date: !binary |-
  MjAxMS0wOC0yNSAxNTo1OToyNiArMDAwMA==
date_gmt: !binary |-
  MjAxMS0wOC0yNSAyMDo1OToyNiArMDAwMA==
categories:
- Uncategorized
tags: []
comments: []
---
<p>It's been almost 2 years since I've posted anything here... and quite a bit longer since I've posted anything that's at all interesting :-\  However, I've begun a new blog over at <a href="http://jayknight.com/">http://jayknight.com/</a>.  As you'll quickly see, that blog is going to be about Christian-y things... things that have to do with my searching for, and likely conversion to, Orthodox Christianity.  I do intend to keep this blog going as well and post interesting things I find online or geeky things regarding programming, photography, and maybe a bit of politics. So stay tuned (but don't hold your breath).</p>
