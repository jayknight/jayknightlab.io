---
layout: post
status: publish
published: true
title: More Microsummaries
author:
  display_name: Jay
  login: jk3us
  email: jay@jayknight.com
  url: ''
author_login: jk3us
author_email: jay@jayknight.com
wordpress_id: 46
wordpress_url: http://jk3.us/2006/07/20/more-microsummaries/
date: !binary |-
  MjAwNi0wNy0yMCAxNDo1MToxOCArMDAwMA==
date_gmt: !binary |-
  MjAwNi0wNy0yMCAyMDo1MToxOCArMDAwMA==
categories:
- Uncategorized
tags:
- programming
- web
comments:
- id: 92
  author: Joe la Poutre
  author_email: jsixpack@gmail.com
  author_url: http://squio.nl/blog/
  date: !binary |-
    MjAwNi0wNy0yMSAwODoxMTo0NyArMDAwMA==
  date_gmt: !binary |-
    MjAwNi0wNy0yMSAxNDoxMTo0NyArMDAwMA==
  content: ! 'Hi Jay,


    Yes, those microsummaries are realy cool! I jus experimented with them in a couple
    of ways...


    First, I threw together a simple WordPress plugin to generate a text-only Microsummary
    (inserts the link and generates the link target, so to say). See http://squio.nl/blog/2006/07/19/microsummaries-plugin-for-wordpress/


    Further I figured out that a Greasemonkey script could be really helpful for creating
    Microsummaries, see http://squio.nl/blog/2006/07/21/create-microsummaries-with-greasemonkey/


    Please share your thoughts on these, and happy micro-summarizing!


    - Joe.'
- id: 93
  author: Jay K
  author_email: jay@jk3.us
  author_url: http://jk3.us/
  date: !binary |-
    MjAwNi0wNy0yMSAwODozMToxOCArMDAwMA==
  date_gmt: !binary |-
    MjAwNi0wNy0yMSAxNDozMToxOCArMDAwMA==
  content: ! 'Joe... very nice.  The problem with the greasemonkey script is that
    the current version of greasemonkey is not compatible with ff2.0b1.  But when
    it is, I''ll be sure try that out.


    Very nice on the wordpress plugin.  I do kinda like the way my (non-plugin) version
    works where it uses 2 different generators depending on whether you bookmark the
    front page (latest headline) or an individual article (number of comments).


    Anyway, keep up the good work'
- id: 94
  author: Joe la Poutre
  author_email: jsixpack@gmail.com
  author_url: http://squio.nl/blog/
  date: !binary |-
    MjAwNi0wNy0yMSAwOTowMjowMCArMDAwMA==
  date_gmt: !binary |-
    MjAwNi0wNy0yMSAxNTowMjowMCArMDAwMA==
  content: ! 'Jay,


    About Greasemokey: there''s a quick hack to install it under 2.0 (see http://joe.lapoutre.com/BoT/Greasemonkey/gm_for_ff20beta.html)


    And a link to my Microsummaries:

    http://joe.lapoutre.com/BoT/Firefox/Microsummaries/


    Cheers, Joe.'
- id: 95
  author: Gopalarathnam
  author_email: gopal@gopalarathnam.com
  author_url: http://gopalarathnam.com/
  date: !binary |-
    MjAwNi0wOC0yMCAwOTowMDoxNSArMDAwMA==
  date_gmt: !binary |-
    MjAwNi0wOC0yMCAxNTowMDoxNSArMDAwMA==
  content: ! 'Can you add a link to my microsummaries? http://gopalarathnam.com/software/


    Thanks.'
---
<p>This <a href="http://jk3.us/2006/07/12/microsummaries/">Microsummary</a> thing is fun!  I keep coming across sites/pages that should have them, so I keep makin em.  I threw up <a title="Jay's Microsummaries" href="http://dev.jk3.us/microsummaries/">a little script</a> to allow viewing and installing the ones I've made.  I propose a wiki-ish microsummary generator site to allow folks to submit ones they've made for others' use.  But I'll just keep putting mine up here for now.</p>
<p>Also, I'll be putting up links to other peoples lists of generators.  If you've got one let me know.</p>
