---
layout: post
status: publish
published: true
title: ! 'Google Earth: Memphis in 3D'
author:
  display_name: Jay
  login: jk3us
  email: jay@jayknight.com
  url: ''
author_login: jk3us
author_email: jay@jayknight.com
wordpress_id: 131
wordpress_url: http://jk3.us/?p=131
date: !binary |-
  MjAwOS0wMy0wMiAxNToyODowMyArMDAwMA==
date_gmt: !binary |-
  MjAwOS0wMy0wMiAyMDoyODowMyArMDAwMA==
categories:
- General
tags:
- geo
- web
comments: []
---
<p>This is the view from my office taken with my G1:</p>
<p><a href="https://www.flickr.com/photos/jk3us/3323806860/"><img src="https://farm4.staticflickr.com/3235/3323806860_a9388d465f_n.jpg" alt="Photo View" /></a></p>
<p>This is the view from my office according to Google's new 3D buildings in Downtown Memphis:</p>
<p><a href="https://www.flickr.com/photos/jk3us/3322970307/"><img src="https://farm4.staticflickr.com/3653/3322970307_47478025f8_m.jpg" alt="Photo View" /></a></p>
<p>Have I ever mentioned that technology is freaking amazing?</p>
