---
layout: post
status: publish
published: true
title: Gmail+talk
author:
  display_name: Jay
  login: jk3us
  email: jay@jayknight.com
  url: ''
author_login: jk3us
author_email: jay@jayknight.com
wordpress_id: 32
wordpress_url: http://jk3.us/?p=32
date: !binary |-
  MjAwNi0wMi0wNyAxMDo0NDozMyArMDAwMA==
date_gmt: !binary |-
  MjAwNi0wMi0wNyAxNjo0NDozMyArMDAwMA==
categories:
- Uncategorized
tags:
- jabber
- web
comments: []
---
<p>Google has <a href="http://mail.google.com/support/bin/answer.py?answer=33506&amp;topic=8405">started rolling out</a> chat features directly in the Gmail webmail interface.  You can (optionally) archive all of your google talk conversations, and will soon be able to chat on the google talk jabber network from the web interface.  Kinda neat... Are they trying to merge the worlds of email and IM?</p>
