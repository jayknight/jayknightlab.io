---
layout: post
status: publish
published: true
title: ! 'del.icio.us: tags for two'
author:
  display_name: Jay
  login: jk3us
  email: jay@jayknight.com
  url: ''
author_login: jk3us
author_email: jay@jayknight.com
wordpress_id: 12
wordpress_url: http://jk3.us/?p=12
date: !binary |-
  MjAwNS0wNy0wOSAwMToyNzo0MiArMDAwMA==
date_gmt: !binary |-
  MjAwNS0wNy0wOSAwNjoyNzo0MiArMDAwMA==
categories:
- Uncategorized
tags:
- web
comments:
- id: 8
  author: Andreas Weinberger
  author_email: andreas@jaw5.de
  author_url: http://www.andreasweinberger.de
  date: !binary |-
    MjAwNS0wNy0wOSAwNToxODoxNiArMDAwMA==
  date_gmt: !binary |-
    MjAwNS0wNy0wOSAxMDoxODoxNiArMDAwMA==
  content: ! 'I think you are right, I am using a simple method for some weeks now
    and it is working just fine: when I want to give a bookmark to someone or a group
    of people I just ad a special tag (@+name or @+group-name).

    The new method has some advantages but the danger of spamming could easily spoil
    it.'
- id: 9
  author: yuri
  author_email: yuri.koval@gmail.com
  author_url: ''
  date: !binary |-
    MjAwNS0wNy0wOSAwNjowNzo1NyArMDAwMA==
  date_gmt: !binary |-
    MjAwNS0wNy0wOSAxMTowNzo1NyArMDAwMA==
  content: Spamming must be considered too.
- id: 10
  author: johnk
  author_email: johnk@gmail.com
  author_url: ''
  date: !binary |-
    MjAwNS0wNy0wOSAxNTo1Mjo0MiArMDAwMA==
  date_gmt: !binary |-
    MjAwNS0wNy0wOSAyMDo1Mjo0MiArMDAwMA==
  content: delicious seems to be going the way of the dodo, which is kinda sad. Some
    strange decisions coming out of there, recently.
- id: 11
  author: TechCrunch
  author_email: ''
  author_url: http://www.techcrunch.com/?p=79
  date: !binary |-
    MjAwNS0wNy0xMCAxMjoxNjozNiArMDAwMA==
  date_gmt: !binary |-
    MjAwNS0wNy0xMCAxNzoxNjozNiArMDAwMA==
  content: ! "<strong>Profile - Del.icio.us (new feature)</strong>\n\n\tCompany: Deli.cio.us
    (new feature)\n\n\tPrevious Profile: June 16, 2005\n\tWhat&#8217;s new?\n\tJosh
    Schachter (the creator of del.icio.us) announced new &#8220;tags for two&#8221;
    functionality yesterday. It allows direct bookmarket notification to a perso..."
- id: 12
  author: Bubbling Minds &raquo; del.icio.us for two
  author_email: ''
  author_url: http://net.cvusonderjylland.dk/bubblingminds/index.php?p=599
  date: !binary |-
    MjAwNS0wNy0xMSAwOTo1MTowNyArMDAwMA==
  date_gmt: !binary |-
    MjAwNS0wNy0xMSAxNDo1MTowNyArMDAwMA==
  content: ! "[...] til hinanden ved at tagge en side http://del.icio.us/for:username
    Der er selvfølgelig stor bekymring for, når spammen rammer denne funktion!  \t\r
    \t\r \t\r   \t\r     \tArkiveret under:  \tSocial  [...]"
- id: 13
  author: GuiM Weblog
  author_email: ''
  author_url: http://guim.typepad.com/blog/2005/07/tags_pour_vous_.html
  date: !binary |-
    MjAwNS0wNy0xNiAwNDoyMjo1NiArMDAwMA==
  date_gmt: !binary |-
    MjAwNS0wNy0xNiAwOToyMjo1NiArMDAwMA==
  content: ! "<strong>Tags pour vous, tags pour nous</strong>\n\n Une application
    de la technologie des t"
---
<p><a href="http://del.icio.us/">del.icio.us</a> has just introduced <a href="http://blog.del.icio.us/blog/2005/07/tags_for_two.html">tagging items for others</a>.  This is pretty cool, I guess.  It could be done before by just asking your friends to use a certain tag when they wanted to share a site with you.  This kinda gets away from a true  <a href="http://technorati.com/tag/folksonomy" rel="tag">folksonomy</a>: they're telling you how certain tags should be used.  I would rather see this happen organically in the community with no help from the admins.  Also, this could open the door for spam... and spam isn't what comes to mind when I think of del.icio.us.</p>
