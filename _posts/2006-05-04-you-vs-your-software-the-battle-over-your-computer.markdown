---
layout: post
status: publish
published: true
title: ! 'You vs. Your software:  The Battle Over Your Computer'
author:
  display_name: Jay
  login: jk3us
  email: jay@jayknight.com
  url: ''
author_login: jk3us
author_email: jay@jayknight.com
wordpress_id: 39
wordpress_url: http://jk3.us/2006/05/04/you-vs-your-software-the-battle-over-your-computer/
date: !binary |-
  MjAwNi0wNS0wNCAxMzoyMTo0MiArMDAwMA==
date_gmt: !binary |-
  MjAwNi0wNS0wNCAxOToyMTo0MiArMDAwMA==
categories:
- Uncategorized
tags:
- life
- security
comments: []
---
<p>Go read <a title="Who Owns Your Computer?" href="http://www.schneier.com/blog/archives/2006/05/who_owns_your_c.html">the excellent article</a> by Bruce Schneier discussing who actually has control over your computer and who you can trust.  If you don't typically think about this stuff, now is a good time to start:</p>
<blockquote><p>There's a battle raging on your computer right now -- one that pits you against worms and viruses, Trojans, spyware, automatic update features and digital rights management technologies. It's the battle to determine who owns your computer.</p></blockquote>
<p>Installing Software on your computer (or sometimes just sticking a CD in) is roughly analogous to letting someone live in your house.  Once they're invited in, they have access to anything in it while you're not looking.  They may even secretly leave unwanted gifts behind after they're gone.  Be careful about what makes its way inside.</p>
<p>Go now, and take your computer back.</p>
