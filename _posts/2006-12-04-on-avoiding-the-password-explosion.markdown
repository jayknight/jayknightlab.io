---
layout: post
status: publish
published: true
title: On Avoiding the Password &#039;Explosion&#039;
author:
  display_name: Jay
  login: jk3us
  email: jay@jayknight.com
  url: ''
author_login: jk3us
author_email: jay@jayknight.com
wordpress_id: 54
wordpress_url: http://jk3.us/2006/12/04/on-avoiding-the-password-explosion/
date: !binary |-
  MjAwNi0xMi0wNCAxNTowNDo1NyArMDAwMA==
date_gmt: !binary |-
  MjAwNi0xMi0wNCAyMTowNDo1NyArMDAwMA==
categories:
- Uncategorized
tags:
- security
- web
comments:
- id: 252
  author: Profpatsch
  author_email: mail@profpatsch.de
  author_url: http://profpatsch.de
  date: !binary |-
    MjAxMy0xMC0wMiAxMzoxMDo0OCArMDAwMA==
  date_gmt: !binary |-
    MjAxMy0xMC0wMiAxODoxMDo0OCArMDAwMA==
  content: ! "I’ve got a 20-something password in my muscle memory.\r\n\r\nSo when
    I need a new password for a service I think of the first word that comes to my
    mind looking at the name (most often a rhyme) and concat it with my “master” string.\r\n\r\n25+
    letters with no cognitive overhead. There you go."
---
<p><a href="http://news.bbc.co.uk/2/hi/technology/6199372.stm">This article</a> from the BBC states:</p>
<blockquote><p>The number of passwords and logins web users need makes it inevitable they will re-use phrases, warned the International Telecommunications Union.</p>
<p>Re-using these identifiers puts people at serious risk of falling victim to identity theft, said the ITU report.</p>
<p>It called on regulators and businesses to find better ways for people to identify themselves to websites.</p></blockquote>
<p>This just re-iterates what I've said before: "<a href="http://jk3.us/2005/07/14/time-is-ripe-for-distibuted-authentication/">Time is ripe for distributed authentication</a>."</p>
<p>OpenID already exists, is fairly well proven to avoid these problems and has support in several programming languages and content management systems.  The only barrier to overcome is getting joe internet user to understand how it works and how it benefits  them.</p>
<p>But whatever you do, please don't leave it up to regulators.</p>
