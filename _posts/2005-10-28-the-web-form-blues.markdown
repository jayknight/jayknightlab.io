---
layout: post
status: publish
published: true
title: The Web Form Blues
author:
  display_name: Jay
  login: jk3us
  email: jay@jayknight.com
  url: ''
author_login: jk3us
author_email: jay@jayknight.com
wordpress_id: 27
wordpress_url: http://jk3.us/?p=27
date: !binary |-
  MjAwNS0xMC0yOCAxNTo1ODo0NiArMDAwMA==
date_gmt: !binary |-
  MjAwNS0xMC0yOCAyMDo1ODo0NiArMDAwMA==
categories:
- Uncategorized
tags:
- web
comments: []
---
<p>My current job title is "Web Developer."  I develop web applications, and I generally enjoy it quite a bit.  But the applications that I work with on a daily basis tend to include forms.  Yuck.  We have talked about it time and again in the office, and I have scoured the web for an elegent way to deal with them.</p>
<p>We've tried Pear's <a href="http://pear.php.net/package/HTML_QuickForm">QuickForm</a> package, and it's okay for simple forms, but having full control over each element just isn't possible.  QuickForm takes the approch of generating the markup and adding validation rules with php code.  That is you never write html when creating such a form, it does it for you.  Sometimes you really just need to write you own markup.</p>
<p>A while back I came across <a href="http://simon.incutio.com/archive/2003/06/17/theHolyGrail">this article</a> that outlined a pretty neat way to deal with it.  I really like the idea of including the rules in the markup and have php use those rules to validate the form.  This particular implemenation adds a bunch of tags and elements in the html, which just doesn't sit right with me.</p>
<p>So what if we could use completely standard markup, yet still include these validation rules in it.  If only there was a language that described forms this richly...  Enter <a href="http://www.w3.org/MarkUp/Forms/">XForms</a>.  XForms has all that built in.  What if we could write an XForm, have php (or your language of choice)  parse it AND use the built in rules to validate it on submit?  For now, we'd have to have php spit out old-style html forms that correspond to the structure in the XForm... but we'd be ready for the future, and someday we'd be able to take out that part and just send the XForm.</p>
<p>I think I'll familiarize myself a bit more with the XForms recomendations and outline a library to do this.  Thoughts?</p>
