---
layout: post
status: publish
published: true
title: Lighting Up Lady Liberty
author:
  display_name: Jay
  login: jk3us
  email: jay@jayknight.com
  url: ''
author_login: jk3us
author_email: jay@jayknight.com
wordpress_id: 68
wordpress_url: http://jk3.us/2007/10/10/lighting-up-lady-liberty/
date: !binary |-
  MjAwNy0xMC0xMCAxMzowMTowOCArMDAwMA==
date_gmt: !binary |-
  MjAwNy0xMC0xMCAxOTowMTowOCArMDAwMA==
categories:
- General
tags:
- ron paul
- web
comments: []
---
<p>The last week of August, Dr. Ron Paul's presidential campaign set a goal to raise half a million dollars before the end of the month and the third quarter. After about 3 days, that goal was reached and double to one million dollars. When the quarter ended, the online donations for that last week was $1.2 million and the quarter total was <a href="http://ronpaul2008.typepad.com/ron_paul_2008/2007/10/press-release-t.html">over $5 million</a>! That's really awesome, but the real interesting part is the unprecedented transparency of the funds raised.</p>
<p>For that challenge, they put up a flash app with a thermometer that showed the progress of the fund raising that updated in (near) real time, showing the exact total and the name and location of recent donors. Paul fans web wide were glued to their browser that week watching the amazing speed of the mercury rising.</p>
<p>This quarter, the campaign is continuing the openness by "letting everyone track our progress throughout the quarter - the first campaign in history to disclose immediately every cent that is raised." But not only can you see the current total on <a href="http://ronpaul2008.com/">Ron Paul's site</a>, in the form of "Lighting Up Lady Liberty", but a few people are aggregating this data and making it available in the form of some <a href="http://ronpaulgraphs.com/">neato</a> <a href="http://slact.net/paulcash.php">graphs</a>.</p>
<p>Update: I've made a <a href="http://dev.jk3.us/microsummaries/generators/ronpaulgraphs-total.xml">Live Title generator</a> that will display the total raised for the quarter as the bookmark title.</p>
