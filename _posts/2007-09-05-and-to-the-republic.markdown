---
layout: post
status: publish
published: true
title: And to the Republic
author:
  display_name: Jay
  login: jk3us
  email: jay@jayknight.com
  url: ''
author_login: jk3us
author_email: jay@jayknight.com
wordpress_id: 67
wordpress_url: http://jk3.us/2007/09/05/and-to-the-republic/
date: !binary |-
  MjAwNy0wOS0wNSAxNTowNzo0MyArMDAwMA==
date_gmt: !binary |-
  MjAwNy0wOS0wNSAyMTowNzo0MyArMDAwMA==
categories:
- Uncategorized
tags:
- politics
comments: []
---
<p>My fairly recent newfound interest in politics has led me to read up on and follow things that has always either bored or saddened me (much of it still does).  In the course of keeping up with the latest goings on, I ran across <a href="http://angrier.net/what_form_of_government/">this little blog post</a> noting the differences between a democracy and a republic.  The line between them has been blurred into invisibility in today's culture, but the founders understood the contrast very well and warned against a democratic government.</p>
<p>So, I'll have to learn more about this:  It's nice to have a good, simple, definition-based way to express some of the things that have been swimming aimlessly in my head.  It's also funny to juxtapose these terms with our political parties that claim them as namesakes.  I'm finding it hard to figure out exactly how they justify calling themselves either.</p>
