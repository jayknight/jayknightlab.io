---
layout: post
status: publish
published: true
title: California Trip
author:
  display_name: Jay
  login: jk3us
  email: jay@jayknight.com
  url: ''
author_login: jk3us
author_email: jay@jayknight.com
wordpress_id: 87
wordpress_url: http://jk3.us/?p=87
date: !binary |-
  MjAwOC0xMS0xMSAxMDowODowOSArMDAwMA==
date_gmt: !binary |-
  MjAwOC0xMS0xMSAxNTowODowOSArMDAwMA==
categories:
- General
tags:
- family
- life
- photos
comments: []
---
<p><a title="Vista by jk3us, on Flickr" href="http://www.flickr.com/photos/jk3us/3004168769/"><img class="alignleft" src="http://farm4.static.flickr.com/3180/3004168769_1e8d0a618b_m.jpg" alt="Vista" width="240" height="170" /></a><br />
Janna and I (and Janna's mom) spent last week in California hanging out with our niece and nephews while their parents were out of town.  We had a lot of fun, and did neat things, like going to Death Valley, Hollywood, and  seeing giant sequoia trees: things you don't see in the Mid-South. It reminds me that there are lots of wonderfully unique places in this world, and I hope I get to see a few more of them someday. I've posted a <a href="http://flickr.com/photos/jk3us/archives/date-posted/2008/11/detail">few pictures</a> of the trip up on flickr for your enjoyment.</p>
