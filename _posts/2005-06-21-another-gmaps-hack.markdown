---
layout: post
status: publish
published: true
title: Another Gmaps Hack
author:
  display_name: Jay
  login: jk3us
  email: jay@jayknight.com
  url: ''
author_login: jk3us
author_email: jay@jayknight.com
wordpress_id: 5
wordpress_url: http://jk3.us/2005/06/21/another-gmaps-hack/
date: !binary |-
  MjAwNS0wNi0yMSAxNjozMzoyOCArMDAwMA==
date_gmt: !binary |-
  MjAwNS0wNi0yMSAyMTozMzoyOCArMDAwMA==
categories:
- General
tags:
- links
comments: []
---
<p>Here's <a href="http://www.sueandpaul.com/gmapPedometer/">another gmaps hack</a> that I find very cool.  Double click on waypoints, and it will calculate the length of your route...  Now, I know that it is precisely 5.053571127021144 miles from my house to the office.  Nicely done.</p>
