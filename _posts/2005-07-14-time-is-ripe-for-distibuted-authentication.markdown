---
layout: post
status: publish
published: true
title: Time is ripe for distributed authentication
author:
  display_name: Jay
  login: jk3us
  email: jay@jayknight.com
  url: ''
author_login: jk3us
author_email: jay@jayknight.com
wordpress_id: 13
wordpress_url: http://jk3.us/?p=13
date: !binary |-
  MjAwNS0wNy0xNCAyMTozMDo0MyArMDAwMA==
date_gmt: !binary |-
  MjAwNS0wNy0xNSAwMjozMDo0MyArMDAwMA==
categories:
- Uncategorized
tags:
- security
- web
comments:
- id: 15
  author: Rob Lanphier
  author_email: robla@robla.net
  author_url: http://robla.net
  date: !binary |-
    MjAwNS0wNy0xNSAxNzowOTowNiArMDAwMA==
  date_gmt: !binary |-
    MjAwNS0wNy0xNSAyMjowOTowNiArMDAwMA==
  content: ! 'OpenID does seem to have the momentum right now.  What are some of the
    other solutions you''ve looked into?  <a href="http://spectaclar.org/wiki/Authentication_Systems"
    rel="nofollow">Here''s a list of the solutions I''ve compiled so far</a>, and
    I''d love it if you could add to it anything that you''re aware of.


    Thanks

    Rob'
- id: 16
  author: Administrator
  author_email: jay@jk3.us
  author_url: http://
  date: !binary |-
    MjAwNS0wNy0xNSAxODowNDoxMCArMDAwMA==
  date_gmt: !binary |-
    MjAwNS0wNy0xNSAyMzowNDoxMCArMDAwMA==
  content: ! 'That''s pretty good list.  I don''t claim to be any sort of expert on
    the subject, but I''ve read up a little.  Here are some thoughts on a few items
    from your list:



    Pubcookie: works fairly well for a small set of sites.  We implemented at my alma
    mater for all the many web services available, it''s still dependant (I think)
    on a centralized server, no good for "the whole web"

    LDAP: Would be a great backend for an identity server, but by itself I''m afraid
    it would not be simple enough... would you have to log in as ldap://server.com/cn=Name,dc=server,dc=com,
    or whatever.

    Identity Commons: it looks like they had the right idea, but 1) I can''t find
    specs and 2) It looks like their site hasn''t been updated in a year or so.

    OpenSSO: Looks like another one aimed at a smaller set of sites, and not the web
    at large

    Shibboleth: again, not for web at large

    SourceID: no comment... can''t figure anything out from their site.



    Several of these are based on <a href="http://en.wikipedia.org/wiki/SAML" rel="nofollow">SAML</a>,
    which might be a good thing.  XML is good, but may be more than is needed for
    simple identification... it may be good for sharing data between your identity
    server and some web service. Thoughts?'
- id: 17
  author: Laurens Holst
  author_email: lholst@students.cs.uu.nl
  author_url: http://www.grauw.nl/
  date: !binary |-
    MjAwNS0wNy0xNSAxODoyODo0NiArMDAwMA==
  date_gmt: !binary |-
    MjAwNS0wNy0xNSAyMzoyODo0NiArMDAwMA==
  content: ! 'Hashing passwords with a hash function like SHA1 or MD5 before storing
    will of course achieve exactly the same effect, and can easily be implemented...
    â€˜Distributed authenticationâ€™ is not the only solution.


    Problem is, then you are depending on the site author to do The Right Thing. Which
    apparantly wasnâ€™t the case with SFx.


    ~Grauw'
- id: 18
  author: Administrator
  author_email: jay@jk3.us
  author_url: http://
  date: !binary |-
    MjAwNS0wNy0xNSAxODozNjozNiArMDAwMA==
  date_gmt: !binary |-
    MjAwNS0wNy0xNSAyMzozNjozNiArMDAwMA==
  content: ! 'Grauw, with <b>distributed</b> authentication (with openid, anyway),
    you don''t have to provide your password to the site author at all, just to your
    identity server.  Then there is no way for all the sites you use to misuse the
    data (they don''t have it).


    Also, when it''s distributed, you really truly have a single password to worry
    about... Not just using the same one on every site.  If you think your password
    has been compromised, change it.  Once.  Not "the password of any accounts where
    you use the same password."'
- id: 19
  author: David Roussel
  author_email: jk3@diroussel.xsmail.com
  author_url: ''
  date: !binary |-
    MjAwNS0wNy0xNiAxNzoyNjoxNCArMDAwMA==
  date_gmt: !binary |-
    MjAwNS0wNy0xNiAyMjoyNjoxNCArMDAwMA==
  content: ! 'Note that Drupal already supports distributed authentication.


    For example look in this thread, http://www.spreadfirefox.com/?q=node/view/16836
    search for vwx.  See how the guy has logged in with his drupal.org id into the
    SpreadFirefox.com site.


    Granted it''s not as nice a OpenID, as the password goes through the server.  See
    here for more details http://drupal.org/node/19113'
- id: 20
  author: Administrator
  author_email: jay@jk3.us
  author_url: http://
  date: !binary |-
    MjAwNS0wNy0xNiAxNzo1ODoxNyArMDAwMA==
  date_gmt: !binary |-
    MjAwNS0wNy0xNiAyMjo1ODoxNyArMDAwMA==
  content: David... yes, drupal is a step in the right direction... If that guy used
    his drupal id on all the drupal sites he used (let's assume he uses a lot), then
    a compromised password could be rememdied by a single change.  However, as you
    said, you still enter your password on every site, which almost defeats the purpose
    :)
- id: 21
  author: Jay Knight &raquo; On Avoiding the Password &#8216;Explosion&#8217;
  author_email: ''
  author_url: http://jk3.us/2006/12/04/on-avoiding-the-password-explosion/
  date: !binary |-
    MjAwNi0xMi0wNCAxNTowNTowNSArMDAwMA==
  date_gmt: !binary |-
    MjAwNi0xMi0wNCAyMTowNTowNSArMDAwMA==
  content: ! '[...] This just re-iterates what I&#8217;ve said before: &#8220;Time
    is ripe for distributed authentication.&#8221; [...]'
---
<p>I, along with all member of <a href="http://spreadfirefox.com/">Spread Firefox</a>, received an email explaining that their server had been accessed by an attacker:</p>
<blockquote><p>We don't have any evidence that the attackers obtained personal information about site users, and we believe they accessed the machine to use it to send spam.  However, it is possible that the attackers acquired information site users provided to the site.</p></blockquote>
<p>... and a little later in the email ...</p>
<blockquote><p>We recommend that you change your Spread Firefox password and the password of any accounts where you use the same password as your Spread Firefox account. </p></blockquote>
<p>This is a great reason that a distributed authentication standard needs to be accepted and used across the web... and soon.  A distributed single sign on solution would prevent things like this happening... not preventing servers from being compromised, but preventing attackers from finding usernames and passwords that are potentially (and probably) identical to the credentials used to authenticate on other sites.</p>
<p>Of the potential solutions I've seen, I think I like <a href="http://openid.net/">OpenID</a> the best.  But I would like to see people critique and suggest improvements.  The solution needs to work, of course, but it should also be simple and extensible.</p>
<p>It's time that we stop trusting every site we use with sensitive information.</p>
