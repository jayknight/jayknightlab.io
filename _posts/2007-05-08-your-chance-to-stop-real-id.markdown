---
layout: post
status: publish
published: true
title: Your Chance to Stop REAL ID
author:
  display_name: Jay
  login: jk3us
  email: jay@jayknight.com
  url: ''
author_login: jk3us
author_email: jay@jayknight.com
wordpress_id: 62
wordpress_url: http://jk3.us/2007/05/08/your-chance-to-stop-real-id/
date: !binary |-
  MjAwNy0wNS0wOCAxMjo1NDoyMSArMDAwMA==
date_gmt: !binary |-
  MjAwNy0wNS0wOCAxODo1NDoyMSArMDAwMA==
categories:
- General
tags:
- security
comments: []
---
<p>For the two next hours and a little bit (until 5 PM Eastern, 5/8/07), the Department of Homeland Security is accepting comments from the public about REAL ID.  The Privacy Coalition has <a href="http://www.privacycoalition.org/stoprealid/">a page</a> detailing how you can contact them and let your voice be heard.  So please go and submit your comment to crush this silliness once and for all (What are the chances?).</p>
<p>Edit: <a href="http://www.schneier.com/blog/archives/2007/05/real_id_action.html">Bruce Schneier is smarter than me</a>.</p>
