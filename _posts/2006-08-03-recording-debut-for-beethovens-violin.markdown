---
layout: post
status: publish
published: true
title: Recording Debut for Beethoven&#039;s Violin
author:
  display_name: Jay
  login: jk3us
  email: jay@jayknight.com
  url: ''
author_login: jk3us
author_email: jay@jayknight.com
wordpress_id: 47
wordpress_url: http://jk3.us/2006/08/03/recording-debut-for-beethovens-violin/
date: !binary |-
  MjAwNi0wOC0wMyAxNDo1MDoyOCArMDAwMA==
date_gmt: !binary |-
  MjAwNi0wOC0wMyAyMDo1MDoyOCArMDAwMA==
categories:
- Uncategorized
tags:
- music
- Violin
comments:
- id: 97
  author: Sharee
  author_email: robbos_75@hotmail.com
  author_url: ''
  date: !binary |-
    MjAwNi0wOC0wNyAwMToyNTowNCArMDAwMA==
  date_gmt: !binary |-
    MjAwNi0wOC0wNyAwNzoyNTowNCArMDAwMA==
  content: The CD is called BEETHOVEN. Sonatas for piano and violin Op.23 &amp; Op.30
    no.2. The artists are Andreas Staier, pf and Daniel Sepec, vl. The barcode for
    the CD is 794881802821.
- id: 98
  author: Jay K
  author_email: jay@jk3.us
  author_url: http://jk3.us/
  date: !binary |-
    MjAwNi0wOC0wOCAxNTozOToyMCArMDAwMA==
  date_gmt: !binary |-
    MjAwNi0wOC0wOCAyMTozOToyMCArMDAwMA==
  content: Thanks, Sharee, for that tip.  I found <a href="http://arts.guardian.co.uk/filmandmusic/story/0,,1836220,00.html"
    rel="nofollow">this article</a> about the CD at the Guardian.  They point to an
    amazon.co.uk product page for it, but I can't find it on amazon.com, or anywhere
    else I've looked.  If anyone knows where I can get one of these in the US, let
    me know.
- id: 99
  author: Steven Oliver
  author_email: oliver.steven@gmail.com
  author_url: http://livingwithpenguins.blogspot.com/
  date: !binary |-
    MjAwNy0wMi0wMSAxOToxNjo0NSArMDAwMA==
  date_gmt: !binary |-
    MjAwNy0wMi0wMiAwMToxNjo0NSArMDAwMA==
  content: ! 'http://www.amazon.com/Beethoven-Sonaten-f%C3%BCr-Klavier-Violine/dp/B000G7EYK4

    There''s a link to the CD on Amazon.com for the US.'
- id: 100
  author: Jay K
  author_email: jay@jk3.us
  author_url: http://jk3.us/
  date: !binary |-
    MjAwNy0wMi0wMiAyMDo1MToxNCArMDAwMA==
  date_gmt: !binary |-
    MjAwNy0wMi0wMyAwMjo1MToxNCArMDAwMA==
  content: Yep, that's it Steven.  I got it for Christmas, and it's quite good.  Thanks
    for sharing the link.
---
<p><a href="http://www.cbc.ca/story/arts/national/2006/08/03/beethoven-violin.html">This</a> is pretty neat:</p>
<blockquote><p>Beethoven's own violin has been used in a recording for the first time, according to the Beethoven Foundation.  It was played by German violinist Daniel Sepec on a CD of the composer's violin and piano sonatas.</p></blockquote>
<p>I want this CD, but can't find it anywhere online.  I wish I at least knew the title of it or something.  [Via <a href="http://theclassicalstation.org/classical_news.shtml">WCPE</a>]</p>
