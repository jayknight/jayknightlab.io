---
layout: post
status: publish
published: true
title: Can you change your parents?
author:
  display_name: Jay
  login: jk3us
  email: jay@jayknight.com
  url: ''
author_login: jk3us
author_email: jay@jayknight.com
wordpress_id: 59
wordpress_url: http://jk3.us/2007/02/12/can-you-change-your-parents/
date: !binary |-
  MjAwNy0wMi0xMiAxNzoxMzo0NCArMDAwMA==
date_gmt: !binary |-
  MjAwNy0wMi0xMiAyMzoxMzo0NCArMDAwMA==
categories:
- Uncategorized
tags:
- linux
comments: []
---
<p>Today's Linux tip (technically, a bash tip) deals with one of the most-used commands: cd.</p>
<p>Here's the scenario:  There's a directory deep in the filesystem that you go to a lot, so you put a symlink to it in your home directory.  For some reason, you really need to be in that directory's parent, but 'cd ..' takes you back to your home directory and typing in the whole path is just too hard (aka, you're too lazy).</p>
<p>The solution is the '-P' flag on the cd command: "The -P option says to use the physical directory structure instead of following symbolic links".  So if you cd -P into that directory, you can then cd .. to its parent.  But what if you're already in that directory and you didn't get there with '-P'?  Easy: 'cd -P .'  changes you to the same directory you are in, but in the "real" location, getting rid of any symlinks in your working directory.  Also, 'cd -P ..' will take you to the "real" parent of your current directory.  Such a simple thing, but something that would have saved me some keystrokes in the past.</p>
