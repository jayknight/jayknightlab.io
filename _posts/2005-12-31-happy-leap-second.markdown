---
layout: post
status: publish
published: true
title: Happy Leap Second
author:
  display_name: Jay
  login: jk3us
  email: jay@jayknight.com
  url: ''
author_login: jk3us
author_email: jay@jayknight.com
wordpress_id: 28
wordpress_url: http://jk3.us/?p=28
date: !binary |-
  MjAwNS0xMi0zMSAxODowMDoxOCArMDAwMA==
date_gmt: !binary |-
  MjAwNi0wMS0wMSAwMDowMDoxOCArMDAwMA==
categories:
- General
tags:
- life
comments:
- id: 73
  author: beeman
  author_email: beeman@beeman.nl
  author_url: http://beeman.nl/
  date: !binary |-
    MjAwNi0wMS0wNyAwOTo1NzozMyArMDAwMA==
  date_gmt: !binary |-
    MjAwNi0wMS0wNyAxNTo1NzozMyArMDAwMA==
  content: Wow, i never knew this existed! :o
---
<p><a href="http://www.flickr.com/photos/jk3us/79978824/" title="Leap Second"><img src="http://static.flickr.com/42/79978824_1de03da003_m.jpg" width="238" height="240" alt="Leap Second" /></a></p>
