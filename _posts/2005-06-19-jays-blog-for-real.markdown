---
layout: post
status: publish
published: true
title: Jay&#039;s Blog, For Real!
author:
  display_name: Jay
  login: jk3us
  email: jay@jayknight.com
  url: ''
author_login: jk3us
author_email: jay@jayknight.com
wordpress_id: 126
wordpress_url: http://jk3.us/?p=2
date: !binary |-
  MjAwNS0wNi0xOSAwMjozODoyMyArMDAwMA==
date_gmt: !binary |-
  MjAwNS0wNi0xOSAwNjozODoyMyArMDAwMA==
categories:
- General
- Pictrus
tags: []
comments:
- id: 2
  author: JTk
  author_email: copacetix@gmail.com
  author_url: http://thisdamnblog.com
  date: !binary |-
    MjAwNS0wNi0yMiAwMDo1NDowNSArMDAwMA==
  date_gmt: !binary |-
    MjAwNS0wNi0yMiAwNTo1NDowNSArMDAwMA==
  content: ! 'Jay has a website!  I''ll be checking in regularly.


    P.S.  I want to design your Pictrus website :)'
- id: 3
  author: vctrharm
  author_email: vctrharm@xs4all.nl
  author_url: http://www.harmlog.nl/
  date: !binary |-
    MjAwNS0wNy0yMCAwMzo1MjozMSArMDAwMA==
  date_gmt: !binary |-
    MjAwNS0wNy0yMCAwODo1MjozMSArMDAwMA==
  content: ! '&gt;Oh, and I might try to make it look pretty sometime, no promises

    If you need a designer? I would love to come with a nice function design.

    for more info:


    http://www.harmlog.nl/

    http://www.bandinabox.tk/

    http://radio.deltit.nu'
---
<p>I now have a more permanent web situation.  And you have found it.  My <a href="http://www.bloglines.com/blog/jk3us">Bloglines Blog</a> will be retired, and <a href="http://jk3.us/">this</a> will be the place to catch up on the life of Jay.</p>
<p>This will also serve as the place that I post news about <a href="http://pictr.us/">Pictrus</a>.  Pictrus is a young, open source project that I've started.  The purpose is to create a low-dependancy, lightweight, easy to use local image browser.  It will also feature the ability to upload pictures (pictrs?) directly to <a href="http://flickr.com/">Flickr</a>.  More to come.</p>
<p>Oh, and I might try to make it look pretty sometime, no promises.</p>
