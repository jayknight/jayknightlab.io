---
layout: post
status: publish
published: true
title: Using Google Talk to chat with other networks
author:
  display_name: Jay
  login: jk3us
  email: jay@jayknight.com
  url: ''
author_login: jk3us
author_email: jay@jayknight.com
wordpress_id: 38
wordpress_url: http://jk3.us/2006/03/31/using-google-talk-to-chat-with-other-networks/
date: !binary |-
  MjAwNi0wMy0zMSAxNjo0NToyNCArMDAwMA==
date_gmt: !binary |-
  MjAwNi0wMy0zMSAyMjo0NToyNCArMDAwMA==
categories:
- General
tags:
- jabber
comments: []
---
<p>You don't have to wait for Google to "federate" with AOL before you can enjoy talking to people on the over-popular AIM network (and ICQ, Y!, MSN, IRC, Gadu-gadu, ....).  The <a href="http://googleblog.blogspot.com/2006/01/open-federation-for-google-talk.html">day</a> that jabber opened up server-to-server communication on the jabber network, this has been possible.   The drawback is that, at this point, you'll need to set it up using a thrid-party client.</p>
<p>I just ran across this <a href="http://www.bigblueball.com/forums/google-talk-news/33739-connect-google-talk-aim-msn-yahoo.html">HOWTO</a> on registering for "transports" to other networks with your gmail account using the <a href="http://psi-im.org/">Psi</a> client (and you may just decide to stick with psi after you've tried it).</p>
<p>I can confirm that this works (I tried it with the very reliable jabber.anywise.com server), and I can also confirm that this works using a <a href="https://www.google.com/hosted">Gmail for your domain</a> address, which enables you to use all of gmail's services with your very own whizbang domain name.</p>
<p>So all you "I won't switch until they support AIM" people can go ahead and switch... and if you convince your all friends to switch, you won't even need the transports anymore :)</p>
<p>Tags: <a rel="tag" href="http://technorati.com/tag/google">Google</a>, <a rel="tag" href="http://technorati.com/tag/jabber">Jabber</a></p>
