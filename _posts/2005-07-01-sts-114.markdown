---
layout: post
status: publish
published: true
title: STS-114
author:
  display_name: Jay
  login: jk3us
  email: jay@jayknight.com
  url: ''
author_login: jk3us
author_email: jay@jayknight.com
wordpress_id: 10
wordpress_url: http://jk3.us/?p=10
date: !binary |-
  MjAwNS0wNy0wMSAwOTo0MzozNiArMDAwMA==
date_gmt: !binary |-
  MjAwNS0wNy0wMSAxNDo0MzozNiArMDAwMA==
categories:
- Uncategorized
tags:
- space
comments: []
---
<p>Space Shuttle Discovery is set to launch on July 13, on the first shuttle mission since the Columbia disaster.  The independant safety panel says that NASA has <a href="http://www.theregister.co.uk/2005/07/01/nasa_shuttle_to_fly/">not yet met</a> all of its recommendations, and that the 3 recommendations that haven't been yet are the most fundamental, including the preventing the external fuel tank from dropping debris on lift off.</p>
<p>Regardless, NASA is set to launch.  On one hand, I wish they would do everything in their power to ensure safety for the crew.  After all, another disaster like Columbia would hurt the administration even worse than it already is.  On the other hand, I know (and more importantly, the crew knows) that space travel is a risky business.  The only way to really improve the "quality" of space travel is to do it.  Without mistakes to learn from, we'll never reach our full potential.  Eileen Collins is the commander of the upcoming mission.  In an interiew, she was asked about the risk involved.  She responded</p>
<blockquote>
<p> I am a huge believer in human exploration. Just think about the history of our country and the history of the world. People have flourished around this planet because I think we humans have something inside of us, a need to explore.<br />
[snip]<br />
To me, it is very important for humans to get off the planet and go do these things. Because I believe in this so much, I think that yes, there is risk in space travel, but I think that itâ€™s safe enough that Iâ€™m willing to take the risk. I think itâ€™s much, much safer than what our ancestors did in traveling across the Atlantic Ocean in an old ship. Frankly, I think they were crazy doing that, but, but they wanted to do that, and we need to carry on the human exploration of the universe that we live in. Iâ€™m honored to be part of that and Iâ€™m proud to be part of it. I want to be able to hand on that belief or enthusiasm that I have to the younger generation because I want us to continue to explore.
</p></blockquote>
<p>I think that's great.  Willing to step out, take a chance.  Even if NASA took extra months or years (and money) to fulfill those recommendations, space flight is risky.  But space flight is essential, at least for future space flight.</p>
<p>I'll watch the launch on <a href="http://spaceflight.nasa.gov/realdata/nasatv/index.html">NASA-TV</a> and probably some of the <a href="http://spaceflight.nasa.gov/shuttle/eva/index.html">spacewalks</a> and the landing.  I'll try to watch the shuttle docked with ISS as they soar overhead during the night.  I'm excited... I should have gone into aeronautics.</p>
