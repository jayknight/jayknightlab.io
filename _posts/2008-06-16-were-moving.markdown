---
layout: post
status: publish
published: true
title: We're Moving!
author:
  display_name: Jay
  login: jk3us
  email: jay@jayknight.com
  url: ''
author_login: jk3us
author_email: jay@jayknight.com
wordpress_id: 73
wordpress_url: http://jk3.us/?p=73
date: !binary |-
  MjAwOC0wNi0xNiAxOToxNToyMSArMDAwMA==
date_gmt: !binary |-
  MjAwOC0wNi0xNyAwMToxNToyMSArMDAwMA==
categories:
- General
tags:
- life
comments:
- id: 141
  author: matthew
  author_email: matthew@matthewclark.net
  author_url: ''
  date: !binary |-
    MjAwOC0wNi0xOCAxMjo1MzozMCArMDAwMA==
  date_gmt: !binary |-
    MjAwOC0wNi0xOCAxODo1MzozMCArMDAwMA==
  content: wow, let's still be friends, at least until we finish the planet earth
    series.
- id: 142
  author: JTk
  author_email: jtk@imjtk.com
  author_url: http://imjtk.com
  date: !binary |-
    MjAwOC0wOS0xMiAxMjo0NTo1NSArMDAwMA==
  date_gmt: !binary |-
    MjAwOC0wOS0xMiAxNzo0NTo1NSArMDAwMA==
  content: ! 'Jay - I can''t imagine working for a better organization than St. Jude''s
    - and they are lucky to be getting you as well.


    Good Luck!'
---
<p><img style="float:left; margin: 5px;" class="alignleft size-medium wp-image-74" src="{{ "/img/house.jpg" | prepend: site.baseurl }}" alt="Our New House" />It is absolutely amazing how fast things can happen.  Only a few weeks ago Janna and I decided to start the process of relocating to the Memphis area.  We put <a href="http://cathyrussell.crye-leike.com/detail.php?tid=oxfordms&amp;mlsnum=118322">our house</a> on the market and started to casually look at houses to buy.  Considering all the mess with the housing market, we expected for our house to sit on the market for a month or two until we lowered the price enough for someone to bite.</p>
<p>We could not have been more wrong.  In less than a week, we had an offer that we could simply not turn down and -- despite not really being mentally prepared to do so -- we accepted it.  That put the pressure on, we had to find a place to move to one month later.... So the next weekend (this past weekend) we spent most of the day Saturday looking for a house.  There were a lot of houses that would have been good for us that we could afford, but most were nothing really special.   But in the afternoon we happened upon <a href="http://www.crye-leike.com/main/browsedetail.php?tid=memphis&amp;mlsnum=3141467">one</a> that was simply charming and very fitting for us.  We made another appointment to see it again that night with our parents, and they were as amazed as we were.  The next afternoon we met with our Realtor to put together an offer.  Our realistic expectations did not expect them to come back the next morning and accept.  But, of course, this post is all about us being very wrong.</p>
<p>So, there you have it.... in about three weeks, we have gone from considering moving to selling one house and buying another.  We'll be moving into our wonderful home in Bartlett on July 12.  Let's just hope the rest of the process goes as unexpectedly smooth as it has so far.</p>
<p>We will miss Oxford pretty sorely.  We've been here for four years, and I was mostly here the six years prior to that.  We have had unbelievable oppurtunities here, and have simply fallen in love with this place.  I don't doubt that life will be grand in Bartlett, but I know it will never be Oxford.  We have wonderful friends here that we will be sad to see less often, and our little house has treated us so nice... It will be hard to leave.</p>
<p>If you are one of the two people that reads this blog, we would like to invite you to our new home.  If you're coming from Oxford, bring some Ajax.</p>
