---
layout: post
status: publish
published: true
title: Groundhog Day
author:
  display_name: Jay
  login: jk3us
  email: jay@jayknight.com
  url: ''
author_login: jk3us
author_email: jay@jayknight.com
wordpress_id: 30
wordpress_url: http://jk3.us/?p=30
date: !binary |-
  MjAwNi0wMi0wMiAxMTozNjo1NSArMDAwMA==
date_gmt: !binary |-
  MjAwNi0wMi0wMiAxNzozNjo1NSArMDAwMA==
categories:
- General
tags:
- life
comments: []
---
<p>This is so weird.... Every day when I wake up, it's always Groundhog Day.  Every day is the same... Phil always sees his shadow.  Will winter never end?</p>
<p>How can I escape this repetitive existence?</p>
