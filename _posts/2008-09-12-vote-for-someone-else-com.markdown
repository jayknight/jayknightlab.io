---
layout: post
status: publish
published: true
title: Vote For Someone Else (.com)
author:
  display_name: Jay
  login: jk3us
  email: jay@jayknight.com
  url: ''
author_login: jk3us
author_email: jay@jayknight.com
wordpress_id: 77
wordpress_url: http://jk3.us/?p=77
date: !binary |-
  MjAwOC0wOS0xMiAxMjoyODo1NCArMDAwMA==
date_gmt: !binary |-
  MjAwOC0wOS0xMiAxNzoyODo1NCArMDAwMA==
categories:
- General
tags:
- politics
- ron paul
- web
comments: []
---
<p>Ever since it was pretty clear that Ron Paul would not be the Republican nominee, I've planned on voting for someone who is not John McCain or Barack Obama.  I've learned enough over the past year and a half to realize that neither of those represents the kind of change this country needs, and in fact are exactly the same in all of the important fundamental issues that we face.</p>
<p>The other day, Dr. Paul held a press conference with <span style="text-decoration: line-through">all</span> <a href="http://thirdpartywatch.com/2008/09/12/the-barr-campaign-is-over-says-david-f-nolan/">most</a> of the "other" national candidates and urged voters to reject the two-party system by voting for someone else.  He came together with those four candidates to agree on <a href="http://www.campaignforliberty.com/blog/?p=484">four principles</a> that set them apart from the established candidates.</p>
<p>So, in light of that, I've thrown up a new site: <a href="http://voteforsomeoneelse.com/">voteforsomeoneelse.com</a>.  It's a blog-type thing where I'll be posting news and thoughts that will hopefully convince a few people to not vote for the status quo.   Let me know your thoughts and suggestions.</p>
<p>Special Thanks to: The very fitting <a href="http://imjtk.com/the-independence-day-wordpress-theme-15.php">theme</a> is by an <a href="http://imjtk.com/">old colleague</a>, thanks James!</p>
