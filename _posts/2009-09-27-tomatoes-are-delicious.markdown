---
layout: post
status: publish
published: true
title: Tomatoes are Delicious
author:
  display_name: Jay
  login: jk3us
  email: jay@jayknight.com
  url: ''
author_login: jk3us
author_email: jay@jayknight.com
wordpress_id: 156
wordpress_url: http://jk3.us/?p=156
date: !binary |-
  MjAwOS0wOS0yNyAxODowNjoxNSArMDAwMA==
date_gmt: !binary |-
  MjAwOS0wOS0yNyAyMzowNjoxNSArMDAwMA==
categories:
- General
tags: []
comments: []
---
<p>I just updated my router (<a href="http://www.amazon.com/gp/product/B000BTL0OA?ie=UTF8&amp;tag=jk3us-20&amp;linkCode=as2&amp;camp=1789&amp;creative=390957&amp;creativeASIN=B000BTL0OA">Linksys WRT54GL</a>) with the <a href="http://www.polarcloud.com/tomato">Tomato Firmware</a>. Here are my initial observations:</p>
<ol>
<li>The upgrade was much faster and easier than I was anticipating. After writing down some settings I had with the default firmware, I flashed the router. A couple minutes later, I was in the tomato interface completing the last of the installation steps and configuring my wireless network.</li>
<li>The first thing I noticed was that my laptop (connected via wifi) is noticeably faster loading web pages. I think it had been having trouble with DNS lookups. I don't know exactly what is different now, but that was a nice bonus.</li>
<li>I boosted the wireless transmit power (from 42 to 84 dB), and am now (finally) able to get wifi throughout my house. I walked to the farthest corners of my house and still had a very usable connection.</li>
</ol>
<p>I haven't yet played with the more advanced settings (other than boosting wifi power), but so far I am quite impressed.</p>
