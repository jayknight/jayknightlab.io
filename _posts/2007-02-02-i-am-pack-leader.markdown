---
layout: post
status: publish
published: true
title: I am pack leader
author:
  display_name: Jay
  login: jk3us
  email: jay@jayknight.com
  url: ''
author_login: jk3us
author_email: jay@jayknight.com
wordpress_id: 58
wordpress_url: http://jk3.us/2007/02/02/i-am-pack-leader/
date: !binary |-
  MjAwNy0wMi0wMiAxNzoyOTo1NyArMDAwMA==
date_gmt: !binary |-
  MjAwNy0wMi0wMiAyMzoyOTo1NyArMDAwMA==
categories:
- Uncategorized
tags:
- life
comments: []
---
<p>For some unknown reason, our <a href="http://www.flickr.com/photos/jk3us/149504884/">two dogs</a> recently started getting into fights.  We're not talking the typical dogs-playing-rough type fights, these are I'm-gonna-rip-your-throat-out type fights.  Now, they never tried to attack or hurt us, even when we were using our persons to seperate them.  They would just periodically go into a tense, big-eyed stare off followed by a frenzy of teeth and claws.  We were really starting to get worried about what we might have to do if we couldn't make them stop fighting.</p>
<p>Luckily, we're fans of <a href="http://en.wikipedia.org/wiki/The_Dog_Whisperer">The Dog Whisperer</a> with the great <a href="http://en.wikipedia.org/wiki/Cesar_Millan">Cesar Millan</a>.  We started to employ some of the techniques from that show (with the addition of our favorite dog weapon - a squirt bottle), and have worked them down almost to their old normal selves.  The calm, assertive pack leader mentality really works, especially if you have a squirt bottle in your hands.</p>
