---
layout: post
status: publish
published: true
title: Bloglines&#039; new features
author:
  display_name: Jay
  login: jk3us
  email: jay@jayknight.com
  url: ''
author_login: jk3us
author_email: jay@jayknight.com
wordpress_id: 26
wordpress_url: http://jk3.us/?p=26
date: !binary |-
  MjAwNS0xMC0xMyAxMDowNzozNSArMDAwMA==
date_gmt: !binary |-
  MjAwNS0xMC0xMyAxNTowNzozNSArMDAwMA==
categories:
- Uncategorized
tags:
- web
comments: []
---
<p>I use <a href="http://bloglines.com/">blogines.com</a> to read rss feeds.  I periodically look around to see if there is something better, but I have yet to find one.  That doesn't mean that there aren't some things I would like to see in a reader.  Just recently, I was wishing there were keyboard shortcuts to navigate my many folders of feeds.  And, alas, this morning I noticed this at the bottom of the list of articles:</p>
<p><code>Hotkeys: j - next article k - previous article s - next sub f - next folder A - read all r - refresh left pane</code></p>
<p>It's not huge news, but It makes my life a touch easier, and will help the batteries in my cordless optical mouse last longer :)</p>
