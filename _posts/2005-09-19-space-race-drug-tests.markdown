---
layout: post
status: publish
published: true
title: Space Race Drug Tests
author:
  display_name: Jay
  login: jk3us
  email: jay@jayknight.com
  url: ''
author_login: jk3us
author_email: jay@jayknight.com
wordpress_id: 24
wordpress_url: http://jk3.us/?p=24
date: !binary |-
  MjAwNS0wOS0xOSAxNTo1Nzo0MyArMDAwMA==
date_gmt: !binary |-
  MjAwNS0wOS0xOSAyMDo1Nzo0MyArMDAwMA==
categories:
- Uncategorized
tags:
- space
comments: []
---
<p>NASA just unveiled its plan to return to the moon by 2020.  It involves vehicles and systems that look and act a lot like those used in the Apollo missions.  Michael Griffin referred to it as "Apollo on Steroids."  But much of the technology is the same as that of the current space shuttle.  Both the SRBs and SSMEs will be used on both vehicles.</p>
<p>The new plan involves two seperate vehicles - one for cargo and one for crew.  The advantages of this are that the crew vehicle can be smaller, cheaper, and safer and the cargo vehicle can have a much higher capacity (over four times higher).</p>
<p>They expect to be using the new vehicles to go to ISS as soon as 2010, which is good, because that's when the shuttle is scheduled to retire.</p>
<p>So, this is great, right?   Some new vehicles, some new goals.  My only question is how come we did it in 8 years in the 60s, but now it's gonna take 13?  I mean, I respect the fact that good engineering takes time, but it seems like the length would decrease over time?  Will NASA be beat to the punch by another space agency?  <a href="http://en.wikipedia.org/wiki/Scaled_Composites">Someone else</a>?</p>
