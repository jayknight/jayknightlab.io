---
layout: post
status: publish
published: true
title: Open Source Linux Phone
author:
  display_name: Jay
  login: jk3us
  email: jay@jayknight.com
  url: ''
author_login: jk3us
author_email: jay@jayknight.com
wordpress_id: 48
wordpress_url: http://jk3.us/2006/08/16/open-source-linux-phone/
date: !binary |-
  MjAwNi0wOC0xNiAxMjoxOToxMiArMDAwMA==
date_gmt: !binary |-
  MjAwNi0wOC0xNiAxODoxOToxMiArMDAwMA==
categories:
- Uncategorized
tags:
- linux
- programming
comments: []
---
<p>Trolltech will be shipping a linux-based phone for developers in September.  The device will be sold with a developer kit with everything (presumably) you need to write/rewrite software for it.  It will connect to cell networks via GSM/GPRS and have builtin wifi.  (Story <a href="http://www.linuxdevices.com/news/NS8030785497.html">here</a>).</p>
<p>Depending on the price, I really think I want one of these.  I'm thinking of using the wifi for communications over jabber+jingle and/or SIP in addition to web and email.</p>
<p>If I end up getting one, you can bet that I'll be posting about it... you know, my birthday is in September ;-)</p>
