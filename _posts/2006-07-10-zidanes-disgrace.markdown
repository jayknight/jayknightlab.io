---
layout: post
status: publish
published: true
title: Zidane&#039;s Disgrace
author:
  display_name: Jay
  login: jk3us
  email: jay@jayknight.com
  url: ''
author_login: jk3us
author_email: jay@jayknight.com
wordpress_id: 42
wordpress_url: http://jk3.us/2006/07/10/zidanes-disgrace/
date: !binary |-
  MjAwNi0wNy0xMCAxNjozODozMCArMDAwMA==
date_gmt: !binary |-
  MjAwNi0wNy0xMCAyMjozODozMCArMDAwMA==
categories:
- Uncategorized
tags:
- life
comments: []
---
<p>In case you missed it, Italy's Word Cup championship was almost overshadowed by France's Zinedine Zidane's <a href="http://www.google.com/news?hl=en&amp;ned=us&amp;ie=UTF-8&amp;q=zidane+red+card&amp;btnG=Search+News">vicious headbutt</a> to Italy's Marco Materazzi.  The attack, which was replayed over and over all around the world earned Zidane a red card, ejecting him from what was most likely his last international soccer appearance.</p>
<p>If you missed the game (you know how American's hate to miss soccer games), <a href="http://youtube.com/results?search=zidane+red+card+world+cup&amp;search_type=search_videos&amp;search=Search">take your pick</a> from the plethora of videos of it hosted on youtube.</p>
