---
layout: post
status: publish
published: true
title: Using vim as a pager (in gentoo)
author:
  display_name: Jay
  login: jk3us
  email: jay@jayknight.com
  url: ''
author_login: jk3us
author_email: jay@jayknight.com
wordpress_id: 57
wordpress_url: http://jk3.us/2007/02/01/vim-as-pager/
date: !binary |-
  MjAwNy0wMi0wMSAxMToyNjoyMSArMDAwMA==
date_gmt: !binary |-
  MjAwNy0wMi0wMSAxNzoyNjoyMSArMDAwMA==
categories:
- General
tags:
- gentoo
comments:
- id: 111
  author: Steven Oliver
  author_email: oliver.steven@gmail.com
  author_url: http://livingwithpenguins.blogspot.com/
  date: !binary |-
    MjAwNy0wMi0wMSAxOToxMToyNyArMDAwMA==
  date_gmt: !binary |-
    MjAwNy0wMi0wMiAwMToxMToyNyArMDAwMA==
  content: I normally wouldn't comment on something of this nature, but this is one
    of the coolest things I've seen in a long time. Much more interesting than most
    of the crap you read in blogs (including mine for that matter lol). Nice.
- id: 112
  author: Marc
  author_email: marc31boss@gmail.com
  author_url: ''
  date: !binary |-
    MjAwOS0wMi0wOSAxNjozNDowNiArMDAwMA==
  date_gmt: !binary |-
    MjAwOS0wMi0wOSAyMTozNDowNiArMDAwMA==
  content: There's also Most... This is a very good pager, with color support.
- id: 113
  author: Less Colors For Man Pages &laquo; Helpful Linux Tidbits
  author_email: ''
  author_url: http://linuxtidbits.wordpress.com/2009/03/23/less-colors-for-man-pages/
  date: !binary |-
    MjAxMC0wMS0yMiAxNDo0Nzo1OSArMDAwMA==
  date_gmt: !binary |-
    MjAxMC0wMS0yMiAxOTo0Nzo1OSArMDAwMA==
  content: ! '[...] 23, 2009 at 7:32 am (Linux)  Man pages by default use less for
    displaying. I&#8217;ve used vim before to for colored text in man pages but something
    got bjorked in an update. You can color man pages [...]'
---
<p>Wondering what the "vim-pager" use flag on app-editors/vim did, I gave it a shot.  Turns out it gives you 2 extra commands: vimpager and vimmanpager.  So adding<br />
<code><br />
export MANPAGER=/usr/bin/vimmanpager<br />
</code><br />
to your login script of choice (.bashrc works just fine), man will use vim as a pager, complete with systax highlighting and ability to use the mouse scroll wheel and other such fun things to navigate man pages.</p>
<p>You could also set PAGER (and/or alias less) to vimpager, but I found that vimpager waits for EOF before displaying anything, meaning it won't work for commands that run until interupted, and commands that run for a long time wait until completion to show anything at all.</p>
<p>UPDATE to ask: If anyone knows a way to use this but to use the colors from a program with color output, eg. "ls --color=always | vimpager", please let me know.</p>
