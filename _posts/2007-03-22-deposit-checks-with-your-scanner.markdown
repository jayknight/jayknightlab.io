---
layout: post
status: publish
published: true
title: Deposit checks with your scanner
author:
  display_name: Jay
  login: jk3us
  email: jay@jayknight.com
  url: ''
author_login: jk3us
author_email: jay@jayknight.com
wordpress_id: 61
wordpress_url: http://jk3.us/2007/03/22/deposit-checks-with-your-scanner/
date: !binary |-
  MjAwNy0wMy0yMiAxNDo1Mjo1MCArMDAwMA==
date_gmt: !binary |-
  MjAwNy0wMy0yMiAyMDo1Mjo1MCArMDAwMA==
categories:
- Uncategorized
tags:
- life
- web
comments:
- id: 127
  author: Janna Knight
  author_email: Reidor71@aol.com
  author_url: ''
  date: !binary |-
    MjAwNy0wNS0wNSAyMjozNjo1NSArMDAwMA==
  date_gmt: !binary |-
    MjAwNy0wNS0wNiAwNDozNjo1NSArMDAwMA==
  content: USAA is the best!  Go Navy!!
- id: 128
  author: Jami
  author_email: celebrategsus@yahoo.com
  author_url: ''
  date: !binary |-
    MjAwOS0wOC0yMCAwODoxNDo1MyArMDAwMA==
  date_gmt: !binary |-
    MjAwOS0wOC0yMCAxMzoxNDo1MyArMDAwMA==
  content: Who offers this for business accounts?
---
<p>Ever since my wife and I started using <a href="http://usaa.com/">USAA</a> for banking and auto insurance, we've been nothing but pleased.  Their website is very user-friendly and feature-packed, and their customer service is literally the best I've ever dealt with.  The one drawback was that, since they didn't have any branches in my area, we had to mail in all our checks to be deposited.  They did provide postage-paid envelopes with deposit slips to make the process easy, but it took about a week for the money to show up in our account after we sent it off.... until recently.</p>
<p>Around the beginning of this year, USAA released a feature they call "<a href="https://www.usaa.com/inet/ent_utils/McStaticPages?key=bank_deposit">Deposit@Home</a>" (admittedly not the most original name).  This service allows you to deposit checks by scanning them using an applet on their website.  You sign your check and scan in the front and back and the money is instantly available in your account.... Amazing.</p>
<p>If you're eligible for USAA membership, I highly recommend them for any service they offer.  What other banks have a service like this (I'm sure there are others)?</p>
