---
layout: post
status: publish
published: true
title: The Future of OpenID
author:
  display_name: Jay
  login: jk3us
  email: jay@jayknight.com
  url: ''
author_login: jk3us
author_email: jay@jayknight.com
wordpress_id: 60
wordpress_url: http://jk3.us/2007/03/13/the-future-of-openid/
date: !binary |-
  MjAwNy0wMy0xMyAwNzozOToxMyArMDAwMA==
date_gmt: !binary |-
  MjAwNy0wMy0xMyAxMzozOToxMyArMDAwMA==
categories:
- Uncategorized
tags:
- web
comments: []
---
<p>If you haven't, you really should go listen to <a href="http://www.archive.org/details/thefutureofopenid">Simon Willison's talk on OpenID</a>.  He does a very good job of explaining what it does and how it can be used.  With the slew of big names announcing support for openid, it's clear to see that it is ready to take off.  So go watch the presentation, especially if you're confused or skeptical :)</p>
