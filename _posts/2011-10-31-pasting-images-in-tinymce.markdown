---
layout: post
status: publish
published: true
title: Pasting images in tinymce
author:
  display_name: Jay
  login: jk3us
  email: jay@jayknight.com
  url: ''
author_login: jk3us
author_email: jay@jayknight.com
wordpress_id: 198
wordpress_url: http://jk3.us/?p=198
date: !binary |-
  MjAxMS0xMC0zMSAxNjoyMTowNiArMDAwMA==
date_gmt: !binary |-
  MjAxMS0xMC0zMSAyMToyMTowNiArMDAwMA==
categories:
- Uncategorized
tags:
- webdev
- tinymce
- html5
comments:
- id: 228
  author: Matt
  author_email: mbrowne83@gmail.com
  author_url: ''
  date: !binary |-
    MjAxMi0wMS0yMCAxMToyMDo1MCArMDAwMA==
  date_gmt: !binary |-
    MjAxMi0wMS0yMCAxNzoyMDo1MCArMDAwMA==
  content: ! "Great little script, thanks! I changed it so that it saves the image
    as a jpeg, since on my Mac at least, when you copy an image to the clipboard it's
    always in png format.\r\n\r\nI also made it generate the image filename from the
    alt attribute if it was filled in, and added an $overwrite option if you want
    it to always overwrite existing images.\r\n\r\nHere it is:\r\nhttps://gist.github.com/1648519"
- id: 229
  author: Matt
  author_email: mbrowne83@gmail.com
  author_url: ''
  date: !binary |-
    MjAxMi0wMS0yMyAwOTozNDo1NiArMDAwMA==
  date_gmt: !binary |-
    MjAxMi0wMS0yMyAxNTozNDo1NiArMDAwMA==
  content: ! "Also, you can add the following after line 59 if you want the width
    and height attributes to be set based on the actual size of the image:\r\n\r\n$imgEl-&gt;setAttribute('width',
    imagesx($imgDataFromClipboard));\r\n$imgEl-&gt;setAttribute('height', imagesy($imgDataFromClipboard));\r\n\r\nAn
    if statement could be added to first check if the width/height were already set,
    but keep in mind that the image itself wouldn't be resized...perhaps that could
    be added to the script too, in case someone resizes the image in TinyMCE."
- id: 230
  author: Jay
  author_email: jay@jayknight.com
  author_url: ''
  date: !binary |-
    MjAxMi0wMS0yMyAwOTo1MjoxNyArMDAwMA==
  date_gmt: !binary |-
    MjAxMi0wMS0yMyAxNTo1MjoxNyArMDAwMA==
  content: Great idea for resizing the image based on the width/height attributes.  That
    would prevent the 200 pixel image taking 30 seconds to download :)  I'll try to
    update my gist with some of your suggestions.  Thanks!
- id: 232
  author: Minh
  author_email: ultimaspace@yahoo.com
  author_url: ''
  date: !binary |-
    MjAxMi0wMi0yMSAwNDo1ODoxMiArMDAwMA==
  date_gmt: !binary |-
    MjAxMi0wMi0yMSAxMDo1ODoxMiArMDAwMA==
  content: Can you help me with more details how I can use this function?
- id: 233
  author: Jay
  author_email: jay@jayknight.com
  author_url: ''
  date: !binary |-
    MjAxMi0wMi0yMSAwOToxNjo0MiArMDAwMA==
  date_gmt: !binary |-
    MjAxMi0wMi0yMSAxNToxNjo0MiArMDAwMA==
  content: ! "It would probably need to be tweaked for your specific needs (specifically
    where the images are stored), but the <code>$string</code> variable is the value
    coming from a tinymce textfield that might have images dragged into it (Firefox
    only).  This function finds images embedded in the string as data: urls and saves
    the images and replaces the url with the new location.\r\n\r\nHope this helps!"
---
<p>I've been looking for an improved experience with <a href="http://www.tinymce.com/">tinymce</a> for some projects at work.  I'm finding that a lot of the problems we have can be solved with options and plugins that are built into it.  I guess reading the manual can come in handy from time to time. One thing that would really be nice is pasting or dragging images into the editor and being able to save it along with the text.  After a little experimenting, I found that in Firefox you can do just that.  Unfortunately this is a Firefox-only method right now, but it's better than nothing.</p>
<p>If you have an image on the clipboard, and you paste in a tinymce instance, it will insert the image by adding an &lt;img&gt; tag with a <a href="https://developer.mozilla.org/en/data_URIs">data: url</a> as the src attribute (this also works with dragging image files from your computer into the editor).  Since it's embedded in the html, it gets submitted inline when the form is submitted.  Since data: urls don't work with some browsers, and large images would cause slow page rendering, I whipped up this little (php) function to save the images to disk and update the &lt;img&gt; tag to refer to them:</p>
{% gist 1328964 %}
<p>I use a directory based on the id of the object that is being edited and the md5 as the filename.  It's not that fancy, but I think it works.  I haven't done a huge amount of testing on it, so I'm not completely sure that the html-&gt;DOM-&gt;html process won't mess up other stuff in your document.</p>
<p>I'm also thinking about a method of using the <a href="http://www.html5rocks.com/en/tutorials/file/dndfiles/">html5 file API</a> in tinymce to make similar behavior work in other browsers. Any ideas?</p>
