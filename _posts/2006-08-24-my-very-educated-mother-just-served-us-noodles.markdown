---
layout: post
status: publish
published: true
title: My Very Educated Mother Just Served Us Noodles
author:
  display_name: Jay
  login: jk3us
  email: jay@jayknight.com
  url: ''
author_login: jk3us
author_email: jay@jayknight.com
wordpress_id: 49
wordpress_url: http://jk3.us/2006/08/24/my-very-educated-mother-just-served-us-noodles/
date: !binary |-
  MjAwNi0wOC0yNCAwOToyOToyMSArMDAwMA==
date_gmt: !binary |-
  MjAwNi0wOC0yNCAxNToyOToyMSArMDAwMA==
categories:
- Uncategorized
tags:
- space
comments: []
---
<p><a href="http://news.com.com/Pluto+demoted/2100-11397_3-6109092.html">It's official</a>, <a href="http://en.wikipedia.org/wiki/Pluto">Pluto</a> is no longer a full-fledged planet.  The new definition finalized by the International Astronomical Union today states that a planet in "a celestial body that (a) is in orbit around the Sun [or another star], (b) has sufficient mass for its self-gravity to overcome rigid body forces so that it assumes a hydrostatic equilibrium (nearly round) shape, and (c) has cleared the neighborhood around its orbit."  Pluto does not meet the third requirement, so will now be a dwarf planet along with Ceres and 2003 UB313 (aka Xena) plus <a href="http://en.wikipedia.org/wiki/Dwarf_planet#List_of_Dwarf_planets">some others</a>.</p>
<p>Personally, I have thought this should be the case for a while now.  Since we keep finding Pluto-like bodies out there, it's hard to see Pluto being more "important" than all these other (even though it may be a bit bigger).</p>
<p>So go rewrite your textbooks... how do you like "Noodles" replacing "Nine Pizzas"?</p>
