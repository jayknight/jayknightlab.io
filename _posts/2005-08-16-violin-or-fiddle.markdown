---
layout: post
status: publish
published: true
title: Violin? or Fiddle?
author:
  display_name: Jay
  login: jk3us
  email: jay@jayknight.com
  url: ''
author_login: jk3us
author_email: jay@jayknight.com
wordpress_id: 18
wordpress_url: http://jk3.us/?p=18
date: !binary |-
  MjAwNS0wOC0xNiAxNToxNzoyNSArMDAwMA==
date_gmt: !binary |-
  MjAwNS0wOC0xNiAyMDoxNzoyNSArMDAwMA==
categories:
- General
tags:
- life
- music
- Violin
comments:
- id: 47
  author: Matthew Clark
  author_email: matthew@matthewclark.net
  author_url: ''
  date: !binary |-
    MjAwNS0wOC0yMiAxNjozMzoxOSArMDAwMA==
  date_gmt: !binary |-
    MjAwNS0wOC0yMiAyMTozMzoxOSArMDAwMA==
  content: ! 'So what are fiddlesticks? And I thought that violins were called that
    because

    it came from a time when the people making the instruments used to paint them
    a

    purple/violet color for the somewhat froo froo aristocracy who commissioned them

    in those days. And that Fiddles were called fiddles because the first fiddlers

    found the violins and didn''t know what they were and just "fiddled" around on

    them till they could play something. You seem to know a lot on the subject matter

    and I''m very interested in continuing to read your insights in the future. I''ll

    turn on the box so I can read about the future sooner that normally possible.

    Hopefully, you know what I mean by "box". ...all that and... you''re so cool.'
- id: 48
  author: Jay K
  author_email: jay@jk3.us
  author_url: http://jk3.us/
  date: !binary |-
    MjAwNS0wOC0yMiAxNjo0ODo1MSArMDAwMA==
  date_gmt: !binary |-
    MjAwNS0wOC0yMiAyMTo0ODo1MSArMDAwMA==
  content: ! 'Those are good questions Mr. Clark.  You sound like you have an unordinarilly
    potent curiosity about the origins of musical instrument nominclature.  I would
    encourage you to pursue that farther; it is an extremely engaging subject.


    Anyway, "fiddlesticks" actually has two origins.  The Norwegians used the word
    in the 16th century to refer simply to the fiddle''s bow.  But the more interesting
    origin is that of the french, where it is actually "fidd les Ticks" which means
    "scratch the Ticks" referring to something that is unusually irritating.


    On your other comments: you are presicely correct.  Job well done!'
- id: 49
  author: Eeuccuso
  author_email: aiguaswa@vtjutgli.com
  author_url: ''
  date: !binary |-
    MjAwOC0xMi0xMyAxMjozNjozNCArMDAwMA==
  date_gmt: !binary |-
    MjAwOC0xMi0xMyAxNzozNjozNCArMDAwMA==
  content: Thanks!
---
<p>I play the violin.  I have for about 15 years now and play for a couple of area orchestras and weddings and such.  Every once in a while, I'll mention to someone that I am a violinist as part of a conversation.  As soon as it leaves my mouth, I cringe, knowing what is to come next. "Now, do you play violin? or do you play fiddle?"  or something to that effect.  Of course, the dreaded comment is accompanied with a huge, satisfied grin.</p>
<p>Let me just settle it once and for all.  There's no difference in the instrument itself.  You could say that a real fiddle needs to be old looking and dusty and a violin is clean and shiny.  But really... same instrument.</p>
<p>So really, it's style.  It's not unlike the difference between baroque violin music and romantic violin music.  It's a style, a genre even.</p>
<p>I grew up taking "classical" voilin lessons, and playing in orchestras and string ensembles.  I can play my share of "fiddle" music and have even had a gig or two playing in an impromptu bluegrass band.</p>
<p>Therefore, the answer to your question is yes.  I do play violin or fiddle.  Now, stop asking.  But if you must ask, don't think you're clever or original for doing so.</p>
