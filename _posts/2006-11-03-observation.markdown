---
layout: post
status: publish
published: true
title: Observation
author:
  display_name: Jay
  login: jk3us
  email: jay@jayknight.com
  url: ''
author_login: jk3us
author_email: jay@jayknight.com
wordpress_id: 53
wordpress_url: http://jk3.us/2006/11/03/observation/
date: !binary |-
  MjAwNi0xMS0wMyAxNzo0NTozMyArMDAwMA==
date_gmt: !binary |-
  MjAwNi0xMS0wMyAyMzo0NTozMyArMDAwMA==
categories:
- Uncategorized
tags:
- life
comments: []
---
<p>I have lots of trouble hearing/comprehending the other party when I'm talking on the phone.  I highly prefer to be able to see their face, especially their mouth, when in a detailed conversation.  A lot of time when I'm on the phone, you'll see me with the phone pressed firmly to one ear and a finger in the other.  Something funny that I've just noticed is that when trying really hard to listen, My eyes will point the direction that the phone is in... phone on left ear, eyes look left.  When I'm concentrating really, really hard, I'll bow my head and close my eyes (turning off one sense can heighten another, right?), but I've noticed that my eyes still look in the direction of the phone.</p>
<p>Just thought that was funny and felt like sharing.</p>
