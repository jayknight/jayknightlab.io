---
layout: post
status: publish
published: true
title: Color Vision
author:
  display_name: Jay
  login: jk3us
  email: jay@jayknight.com
  url: ''
author_login: jk3us
author_email: jay@jayknight.com
wordpress_id: 36
wordpress_url: http://jk3.us/2006/03/22/color-vision/
date: !binary |-
  MjAwNi0wMy0yMiAxMjoyOToxNyArMDAwMA==
date_gmt: !binary |-
  MjAwNi0wMy0yMiAxODoyOToxNyArMDAwMA==
categories:
- Uncategorized
tags:
- life
- space
comments:
- id: 78
  author: Matthew Clark
  author_email: matthew@matthewclark.net
  author_url: ''
  date: !binary |-
    MjAwNi0wMy0yOSAxNDowOTowNCArMDAwMA==
  date_gmt: !binary |-
    MjAwNi0wMy0yOSAyMDowOTowNCArMDAwMA==
  content: ! 'Jay,  that''s cool and interesting. I like the

    way you think.'
---
<p>I've wondered for a long time about why "visible light" is where it is in the EM spectrum, and why it seems to be pretty consistant among life on Earth.  For example, why aren't there animals that can "see" EM radiation that is well outside the boundaries of our definition of visible light (about 400-700 nm in wavelength).  In the wikipedia article on <a href="http://en.wikipedia.org/wiki/Visible_light">visible light</a>:</p>
<blockquote><p>The eyes of many species perceive wavelengths different than the spectrum visible to the human eye. For example, many <a title="Insect" href="http://en.wikipedia.org/wiki/Insect">insects</a>, such as <a title="Bee" href="http://en.wikipedia.org/wiki/Bee">bees</a>, can see light in the <a title="Ultraviolet" href="http://en.wikipedia.org/wiki/Ultraviolet">ultraviolet</a>, which is useful for finding <a title="Nectar (plant)" href="http://en.wikipedia.org/wiki/Nectar_%28plant%29">nectar</a> in <a title="Flower" href="http://en.wikipedia.org/wiki/Flower">flowers</a>. For this reason, plant species whose life cycles are linked to insect pollination may owe their reproductive success to their appearance in ultraviolet light. Thus, the true color of flowers may be in the ultraviolet spectrum.</p></blockquote>
<p>That's an example of other creatures' ability to "see" outside the typical visible spectrum, but it's still really close.  Now, Take a look at <a href="http://en.wikipedia.org/wiki/Image:Atmospheric_electromagnetic_transmittance_or_opacity.jpg">this image</a>, and notice that there's a pretty big hole at visible light.  That means that that portion of the spectrum can get through the atmosphere better than most radiation, except for the huge hole around radio frequencies.  That would suggest that creatures can see that portion of the spectrum, <span style="font-weight: bold">because</span> that's the portion that can get through from the heavens... or is it the other way around?</p>
<p>I'm sure this isn't anything new to the world of science, but I've been wondering about this for a long time.  I still don't have all the info I'd like, but I'm no biologist/opto-whatever-ist.  I'll leave the evolution/creation debate for some other blog (or at least another day) and just mention that little connections like these fascinate me.</p>
