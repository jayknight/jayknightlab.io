---
layout: post
status: publish
published: true
title: ! 'Cosmos 1: The First Solar Sail'
author:
  display_name: Jay
  login: jk3us
  email: jay@jayknight.com
  url: ''
author_login: jk3us
author_email: jay@jayknight.com
wordpress_id: 127
wordpress_url: http://jk3.us/?p=3
date: !binary |-
  MjAwNS0wNi0yMCAxMjoyMjoyMyArMDAwMA==
date_gmt: !binary |-
  MjAwNS0wNi0yMCAxNjoyMjoyMyArMDAwMA==
categories:
- Uncategorized
tags:
- space
comments: []
---
<p>Tomorrow (2005-06-21), <a href="http://www.planetary.org/solarsail/">Cosmos 1</a> will be launched from a submarine in the <a href="http://en.wikipedia.org/wiki/Barents_Sea">Barents Sea</a>.  A few days after entering earth orbit at 825 km, Cosmos 1 will deploy its eight triangular solar sail blades that will eventually be turned toward the sun to start collecting energy.</p>
<p>This mission is expiremental, measuring the effects of sun light on such a craft.  It won't go far, just acheiving a higher orbit.  But the measurements gathered will help researchers create solar sail powered vehicles that could eventually take us to other star systems.</p>
<p>After the the sails are deployed, the satellite should be clearly visible in the night sky.  See <a href="http://www.heavens-above.com/">Heavens Above</a> for passings in your area.</p>
