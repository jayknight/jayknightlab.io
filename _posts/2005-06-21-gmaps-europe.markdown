---
layout: post
status: publish
published: true
title: gMaps Europe
author:
  display_name: Jay
  login: jk3us
  email: jay@jayknight.com
  url: ''
author_login: jk3us
author_email: jay@jayknight.com
wordpress_id: 128
wordpress_url: http://jk3.us/?p=4
date: !binary |-
  MjAwNS0wNi0yMSAxNTozODo0MCArMDAwMA==
date_gmt: !binary |-
  MjAwNS0wNi0yMSAyMDozODo0MCArMDAwMA==
categories:
- Uncategorized
tags:
- links
comments: []
---
<p>Google has started adding maps and aerial photographs for some of Europe.  <a href="http://maps.google.com/maps?ll=48.858143,2.293825&amp;spn=0.007639,0.010622&amp;t=k&amp;hl=en">Here's</a> the Eiffel Tower, and <a href="http://maps.google.com/maps?q=moscow,+russia&amp;ll=55.752429,37.623045&amp;spn=0.007639,0.010622&amp;t=k&amp;hl=en">St. Basil's Cathedral</a>, much less impressive from overhead.  I'm going to have to scan some of my photos from my Russia/Europe trip and <a href="http://www.geobloggers.com/index.cfm">GeoBlog</a> them.  Stay in touch with <a href="http://www.googlesightseeing.com/">Google Sightseeing</a>, as I'm sure they will have many more overseas sights.</p>
